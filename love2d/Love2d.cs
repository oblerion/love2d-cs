/*
MIT License Love2d.cs
Copyright (c) 2022 oblerion

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;  
using Microsoft.Xna.Framework.Content;  
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
namespace love2d;
public abstract class Love2d : Game
{

    private Color _curant_color;
    private SpriteFont _curant_font;
    private float _curant_volume=1;
    
    private GameTime _gt;
    private MouseState _ms; 
    private KeyboardState _ks;
    private SpriteBatch _sb;
    private GraphicsDevice _gd;
    private GraphicsDeviceManager _gdm;
    
	public GameTime Gt{get =>_gt;}
    public Love2d()
    {
        _gdm = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";
        IsMouseVisible = true;
    }
    public void window_setmode(int pw,int ph)
    {
        _gdm.PreferredBackBufferWidth = pw;
        _gdm.PreferredBackBufferHeight = ph;
        _gdm.ApplyChanges();
    }
    public int window_getWidth()
    {
        return _gdm.PreferredBackBufferWidth;
    }
    public int window_getHeight()
    {
        return _gdm.PreferredBackBufferHeight;
    }
    public int math_floor(float f)
    {
        return (int)(f*10)/10;
    }
    public bool keyboard_isDown(Keys k)
    {
        return _ks.IsKeyDown(k);
    }
    public bool keyboard_isUp(Keys k)
    {
        return _ks.IsKeyUp(k);
    }
    public int mouse_getX()
    {
        return _ms.X;
    }
    public int mouse_getY()
    {
        return _ms.Y;            
    }
    public bool mouse_isDown(int b)
    { 
        if(b==2 && _ms.RightButton == ButtonState.Pressed)
            return true;
        else if(b==1 && _ms.LeftButton == ButtonState.Pressed)
            return true;
        else if(b==3 && _ms.MiddleButton == ButtonState.Pressed)
            return true;
        return false;
    }
    public Song audio_newsong(string name)
    {
        // music
        return Content.Load<Song>(name);
    }
    public SoundEffect audio_newsound(string name)
    {
        // sound
        return Content.Load<SoundEffect>(name);
    } 
    public void audio_play(Song song)
    {
        if(MediaPlayer.State != MediaState.Playing)
        {
            MediaPlayer.Play(song);
            MediaPlayer.Volume = _curant_volume;
            MediaPlayer.IsRepeating = true;
        }
    }
    public void audio_play(SoundEffect sound)
    {
        sound.Play(_curant_volume,0,0);
    }
    public void audio_stop()
    {
        MediaPlayer.Stop();
    }
    public void audio_setVolume(float v)
    {
        _curant_volume = v;
    }
    public SpriteFont graphics_newfont(string name)
    {
        SpriteFont f = this.Content.Load<SpriteFont>(name);
        _curant_font = f;
        return f;
    }
    public Texture2D graphics_newimage(string name)
    {
        return this.Content.Load<Texture2D>(name);
    }
    public void graphics_draw(Texture2D t,int x,int y)
    {
        Vector2 v2 = new Vector2(x,y);
        _sb.Draw(t,v2,_curant_color);
    }
    public void graphics_draw(Texture2D t,int x,int y,float rotation)
    {
        Vector2 v2 = new Vector2(x,y);
        Vector2 v3 = new Vector2(t.Width/2,t.Height/2);
        Vector2 v4 = new Vector2(1,1);
        _sb.Draw(t,v2,null,_curant_color,rotation,v3,v4,SpriteEffects.None,0);
    }
    public void graphics_draw(Texture2D t,int id,int x,int y)
    {
        Rectangle r1= new Rectangle(x,y,t.Height,t.Height);
        Rectangle r2= new Rectangle(id*t.Height,0,t.Height,t.Height);
        if(id > 0 && id <= t.Width/t.Height)
        {
            _sb.Draw(t,r1,r2,_curant_color);
        }
        else Trace.WriteLine("WARNING: graphics_draw(t,id,x,y) id <= 0 or id > max");
    }
    public void graphics_draw(Texture2D t,int id,int x,int y,float rotation)
    {
        Vector2 v1= new Vector2(x,y);
        Rectangle r1= new Rectangle(id*t.Height,0,t.Height,t.Height);
        Vector2 v2 = new Vector2(t.Height/2,t.Height/2);
        Vector2 v3 = new Vector2(1,1);
        if(id >= 0 && id < t.Width/t.Height)
        {
            _sb.Draw(t,v1,r1,_curant_color,rotation,v2,v3,SpriteEffects.None,0);
        }
        else Trace.WriteLine("WARNING: graphics_draw(t,id,x,y,rotation) id <= 0 or id > max");
    }
    public void graphics_print(string text,int x,int y)
    {
        Vector2 v2 = new Vector2(x,y);
        if(_curant_font!=null)
        {
            _sb.DrawString(_curant_font, text, v2, _curant_color);
        }
    }
    
    public void graphics_print(string text,int x,int y,float rotation,float scale)
    {
        Vector2 v2 = new Vector2(x,y);
        Vector2 FontOrigin= new Vector2(0,0);//font.MeasureString(text) / 2;
        if(_curant_font!=null)
        {
            _sb.DrawString(_curant_font, text, v2, _curant_color,
                rotation, FontOrigin, scale, SpriteEffects.None, 0);
        }
    }
    public void graphics_setfont(SpriteFont sf)
    {
        _curant_font = sf;
    }
    public void graphics_setColor(Color col)
    {
        _curant_color = col;
    }
    public void graphics_setColor()
    {
        _curant_color = Color.White;
    }
    public void graphics_setAlpha(float a)
    {
        _curant_color *= a;
    }
    public void graphics_rect(string type,int x,int y,int w,int h)
    {
        Texture2D rect = new Texture2D(_gd, w, h);
        Color[] data = new Color[w*h];
        Vector2 coor = new Vector2(x, y);
        int i;
        if(type == "fill")
        {
                for(i=0; i < data.Length; i++) 
                data[i] = _curant_color;
                rect.SetData(data);
        }
        else if(type == "line")
        {
            for(i=0; i < data.Length; i++) 
            {
                if(i<w || i%w == 0 || 
                (i-w+1)%w == 0 || i>(w*h)-w)  
                {
                    data[i] = _curant_color;
                }
            } 
            rect.SetData(data);
        }
        if(type=="fill" || type=="line")
        {
            _sb.Draw(rect,coor,Color.White);
        }
    }
    public void graphics_clear(Color col)
    {
        _gd.Clear(col);
    }
    


    // public class Button
    // {
    //     private int x;
    //     private int y;
    //     private int w;
    //     private int h;
    //     public Button()
    //     {

    //     }
    // }
    public abstract void load();
    public abstract void update(GameTime gameTime);
    public abstract void draw(GameTime gameTime);
//-----------------------------------------------------------
    protected override void Initialize()
    {
        _gd = GraphicsDevice;
        _sb = new SpriteBatch(GraphicsDevice);
        graphics_setColor(Color.White);
        // TODO: Add your initialization logic here
        base.Initialize();
    }

    protected override void LoadContent()
    {
        this.load();
        if(_curant_font==null) 
        {
            Trace.WriteLine("WARNING:load font and set it before print text");
            Trace.WriteLine("use graphics_newfont() to load");
            Trace.WriteLine("use graphics_setfont() for set");
        }
        // TODO: use this.Content to load your game content here
    }

    protected override void Update(GameTime gameTime)
    {
		_gt = gameTime;
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            Exit();
        _ms = Mouse.GetState();
        _ks = Keyboard.GetState();
        this.update(gameTime);
        // TODO: Add your update logic here
        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
		_sb.Begin(samplerState : SamplerState.PointClamp);
			this.graphics_clear(Color.Black);
			this.graphics_setColor(Color.White);
			this.draw(gameTime);
        _sb.End();
        base.Draw(gameTime);
    }
}

